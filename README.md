# Local SSL

This repo simply provides a SSL certificate for use in local development.
Specifically, it uses the names:

- *.local.summit.cloud.vt.edu
- local.summit.cloud.vt.edu

## How it works

This repo contains a simple pipeline that does the following:

1. Uses [certbot](https://certbot.eff.org/) to request a certificate using LetsEncrypt
1. Certbot uses the Route53 plugin (and credentials injected via CI variables)
   to complete the DNS challenge.
1. The new key and certificate are placed into the `/cert` directory
1. The new files are committed and pushed


